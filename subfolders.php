<?php

/**
 * Уровень 2, задача 3 
 * 
 * Скрипт, который в папке /datafiles найдет все файлы, имена которых состоят
 * из цифр и букв латинского алфавита, имеют расширение ixt и выведет на экран
 * имена этих файлов, упорядоченных по имени.
 * 
 * @version 1.1
 */

/**
 * Класс для поиска файлов и хранения результата  
 */
class searchFiles {

    /**
     * Массив для хранения значений типа string
     * @access private
     * @var array
     */
    private $filesArray;

    /**
     * Консруктор класса
     * 
     * Coздание массива {@link $filesArray},вызов функции {@link search($dir)}
     * 
     * @param string $dir 
     * @return void
     */
    public function __construct($dir) {
        $this->filesArray = array();
        $this->search($dir);
    }

    /**
     * Метод класса
     * 
     * Рекурcивный обход вложенных папок
     * Если имя файла содержит в себе только латинские буквы и цифры и
     * имеет расширение "ixt" добавляем его в массив {@link $filesArray}
     * 
     * @access private
     * @param string $dir 
     * @return void
     */
    private function search($dir) {
        $objects = new DirectoryIterator($dir);
        foreach ($objects as $object) {
            if ($object->isDir()) {
                /**
                 * Если текущий элемент DirectoryIterator не является '.' или '..'
                 * вызываем функиию search(текущий элемент)
                 */
                if (!$object->isDot()) {
                    $this->search($dir . '//' . ($object->getFilename()));
                }
            } else if (preg_match('/^[[:alnum:]]+\.ixt$/', $object->getFileName())) {
                $this->filesArray[] = $object->getFileName();
            }
        }
    }

    /**
     * Метод класса
     * 
     * Сортирует массив {@link $filesArray}, используя алгоритм "natural order"
     * 
     * @access public
     * @return array
     */
    public function get() {
        natsort($this->filesArray);
        return $this->filesArray;
    }

}

/**
 * Тест класса searchFiles 
 * 
 * Создаем объект класса, выводим результат
 */
$search = new searchFiles('datafiles');
var_dump($search->get());

